package com.instaig.instagen;

import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.instaig.instagen.serviceImpl.TextImage;
import com.instaig.instagen.serviceImpl.TextReader;

@SpringBootApplication
public class InstagenApplication {

	public static void main(String[] args) throws IOException {
		SpringApplication.run(InstagenApplication.class, args);
		
		TextImage textImage = new TextImage();
		
		try {
			TextImage.main(null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
