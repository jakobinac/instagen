package com.instaig.instagen.test;

import java.awt.Dimension;
import java.awt.GraphicsConfiguration;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.media.j3d.BranchGroup;
import javax.media.j3d.Canvas3D;
import javax.media.j3d.ImageComponent;
import javax.media.j3d.ImageComponent2D;
import javax.media.j3d.Screen3D;

import com.sun.j3d.utils.geometry.ColorCube;
import com.sun.j3d.utils.universe.SimpleUniverse;

public class ZDText {
	
	
	public static void main(String[] args) {
        // create canvas
        GraphicsConfiguration config = SimpleUniverse.getPreferredConfiguration();
        Canvas3D canvas3D = new Canvas3D(config, true);

        // create universe
        SimpleUniverse simpleU = new SimpleUniverse(canvas3D);
        simpleU.getViewingPlatform().setNominalViewingTransform();

        // create scene
        BranchGroup scene = new BranchGroup();
        scene.addChild(new ColorCube(0.4));
        scene.compile();
        simpleU.addBranchGraph(scene);

        // set off screen size
        Screen3D sOff = canvas3D.getScreen3D();
        sOff.setSize(new Dimension(200, 200));
        sOff.setPhysicalScreenWidth(1);
        sOff.setPhysicalScreenHeight(1);

        // render to off screen
        BufferedImage bImage = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
        ImageComponent2D buffer = new ImageComponent2D(ImageComponent.FORMAT_RGBA, bImage);
        buffer.setCapability(ImageComponent2D.ALLOW_IMAGE_READ);
        canvas3D.setOffScreenBuffer(buffer);
        canvas3D.renderOffScreenBuffer();
        canvas3D.waitForOffScreenRendering();

        // write to file
        File output = new File("slikau3d.png");
        try {
            ImageIO.write(canvas3D.getOffScreenBuffer().getRenderedImage(), "png", output);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(output.getAbsolutePath());

        simpleU.cleanup();
    }
}
