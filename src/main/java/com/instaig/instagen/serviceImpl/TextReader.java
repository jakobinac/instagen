package com.instaig.instagen.serviceImpl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class TextReader {
	
	private static ArrayList<String> quotes = new ArrayList<>();
	
	private static ArrayList<String> hashtags = new ArrayList<>();
	
	private static ArrayList<String> cta = new ArrayList<>();
	
	private static ArrayList<String> generatedQuotes = new ArrayList<>();
	
	private static String quotesPath = "material/Description/Quotes.txt";
	
	private static String hashtagsPath = "material/Description/Hashtags.txt";
	
	private static String ctaPath = "material/Description/CTA.txt";
	
	private static String generatedQuotesPath = "material/Description/generatedDescriptions.txt";
	
	public static void main(String[] args) throws IOException {
		try {
			readTextForImages(quotesPath,quotes );
			readTextForImages(hashtagsPath,hashtags );
			readTextForImages(ctaPath,cta );
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		calculateHashTags();
		generateQuotes();
		writeQuotesInFile();
	}
	
	public static void readTextForImages(String source, ArrayList<String> target) throws IOException {
		String line;
		BufferedReader br = new BufferedReader(new FileReader(source));	
		try {
		    	
			while ((line = br.readLine()) != null) {
			    if(line.length() > 0) {
			    	target.add(line);
			    }           
			}	
		} finally {
		    br.close();
		}		
		}

	public static void generateQuotes(){
		Random rand = new Random();
		int counter = 0;
		int lastNumber = 10;
		
		do{
			String quote = "";
			Set<String> randomHashTag = calculateHashTags();
			int quotesNum = rand.nextInt(((quotes.size() -1) - 0) + 1) + 0;
			
			quote+= quotes.get(quotesNum);
			quotes.remove(quotesNum);
			for (String string : randomHashTag) {
				quote+=(" #" + string);
			}
			int ctaNum = rand.nextInt(((cta.size() -1) - 0) + 1) + 0;
			quote+=(" " + cta.get(ctaNum));
			generatedQuotes.add(quote);
			counter++;
		}while(counter < lastNumber );
		
		for (String string : generatedQuotes) {
			System.out.println(string);
		}
 	}
	
	public static Set<String> calculateHashTags(){
		Set<String> randomHashTag = new HashSet<String>();
		Random rand = new Random();
		int startCounter = 0;
		int randomNum = rand.nextInt((12 - 7) + 1) + 7;
		do{
			randomHashTag.add(hashtags.get(rand.nextInt(((hashtags.size() -1) - 0) + 1) + 0));	
			startCounter++;
		}while(startCounter < randomNum);	 
		return randomHashTag;
 	}
	
	public static void writeQuotesInFile() throws IOException{
		BufferedWriter bw = new BufferedWriter(new FileWriter(new File(generatedQuotesPath)));
		for (String string : generatedQuotes) {
			bw.write(string);
			 bw.newLine();
		}
		bw.close();
	}

	
}


