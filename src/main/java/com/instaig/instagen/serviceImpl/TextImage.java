package com.instaig.instagen.serviceImpl;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import org.springframework.stereotype.Component;

@Component
public class TextImage {
		
	public static void main(String arg[]) throws IOException {
		int counter = 1;
		BufferedImage bufferedImage = null;
        List<String> text = fillListWithText();
        for (String t : text) {        	
        	 bufferedImage = getImage(counter);    
             bufferedImage = resize(bufferedImage, 4000	, 4000);           
             renderingImage(bufferedImage,t, counter);
             counter++;
		}

    }
	
	private static void renderingImage(BufferedImage bufferedImage, String key, int number) throws IOException{
		   	Rectangle rect = new Rectangle(bufferedImage.getWidth(), bufferedImage.getHeight());	
	        Graphics graphics = bufferedImage.getGraphics();	       
	        graphics.setColor(Color.WHITE);
	        if(key.contains(",")){
	        	String[] arr = key.split(",");
	        	String row1 = arr[0];
	        	String row2 = arr[1];
	        	drawCenteredString(graphics, row1, rect, new Font("Tahoma", Font.BOLD, 200), -80);
	        	drawCenteredString(graphics, row2, rect, new Font("Tahoma", Font.BOLD, 200), 80);
	        } else {
	        	  drawCenteredString(graphics, key, rect, new Font("Tahoma", Font.BOLD, 160), 80);
	        }
	      
	        ImageIO.write(bufferedImage, "jpg", new File( "material/genPic/img-" +number +".jpg"));
	        System.out.println("Image Created");
	}
	
	private static BufferedImage getImage(int number) {
		BufferedImage img = null;
		try 
		{
		    img = ImageIO.read(new File("material/pic/" +number +".jpg")); 
		    
		} 
		catch (IOException e) 
		{
		    e.printStackTrace();
		}
		return img;
		}
	
	public static void drawCenteredString(Graphics g, String text, Rectangle rect, Font font, int rows) {
	    // Get the FontMetrics
		
	    FontMetrics metrics = g.getFontMetrics(font);
	    
	    // Determine the X coordinate for the text
	    int x = rect.x + (rect.width - metrics.stringWidth(text)) / 2;
	    // Determine the Y coordinate for the text (note we add the ascent, as in java 2d 0 is top of the screen)
	    int y = rect.y + ((rect.height - metrics.getHeight()) / 2) + metrics.getAscent() + rows;
	    // Set the font
	   
	    g.setFont(font);
	    // Draw the String
	    g.drawString(text, x, y);
	}
	
	
	public static BufferedImage resize(BufferedImage img, int newW, int newH) {  
	    int w = img.getWidth();  
	    int h = img.getHeight();  
	    BufferedImage dimg = new BufferedImage(newW, newH, img.getType());  
	    Graphics2D g = dimg.createGraphics();  
	    g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
	    RenderingHints.VALUE_INTERPOLATION_BILINEAR);  
	    g.drawImage(img, 0, 0, newW, newH, 0, 0, w, h, null);  
	    g.dispose();  
	    return dimg;  
	}  
	
	private static List<String> fillListWithText(){
		 List<String> textForImages = new ArrayList<>();
		 
		 textForImages.add("WARRIOR SKILL, STRONG INCOME WITHOUT WORK");
		 textForImages.add("QUIT TALKING, BEGIN DOING");
		 textForImages.add("FEEL THAT ENERGY");
		 textForImages.add("NO MATTER WHAT YOU THINK, MATTER IS WHAT YOU HAVE");
		 textForImages.add("7 FIGURES, IS JUST START");
		 textForImages.add("IF YOU UNDERSTAND LIFE, YOU WILL HAVE LIFE ");
		 textForImages.add("MONEY RESPECT POWER");
		 textForImages.add("WHEN YOU`RE NOT HAPPY, EVERY DAY IS PRISON");
		 textForImages.add("PUT YOUR HEAD UP, SAY I WILL SUCCESS");
		 textForImages.add("RISK DOESN`T EXIST, RISK IS DEFICIT OF KNOWLEDGE");
		 textForImages.add("LET`S TALK ABOUT MONEY");
		 textForImages.add("LIFE IS A GAME, PLAY TO WIN");
		 
		 
		 return textForImages;
	}	 

}
