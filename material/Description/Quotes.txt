
Try not to become a man of success. Rather become a man of value. 
It is better to fail in originality than to succeed in imitation. 
Success is getting what you want, happiness is wanting what you get 
Failure is the condiment that gives success its flavor 
Letting go means to come to the realization that some people are a part of your history, but not a part of your destiny 

Have no fear of perfection - you'll never reach it 

Success is stumbling from failure to failure with no loss of enthusiasm. If A is a success in life, then A equals x plus y plus z. Work is x; y is play; and z is keeping your mouth shut 


Anyone whose goal is 'something higher' must expect someday to suffer vertigo. What is vertigo? Fear of falling? No, Vertigo is something other than fear of falling. It is the voice of the emptiness below us which tempts and lures us, it is the desire to fall, against which, terrified, we defend ourselves. 

Don't spend time beating on a wall, hoping to transform it into a door. 

I'm a success today because I had a friend who believed in me and I didn't have the heart to let him down

Sometimes it takes a good fall to really know where you stand
Our greatest glory is not in never falling, but in rising every time we fall 

The way to get started is to quit talking and begin doing 

It is hard to fail, but it is worse never to have tried to succeed # 

A man is a success if he gets up in the morning and gets to bed at night, and in between he does what he wants to do # 

Only those who dare to fail greatly can ever achieve greatly # 

If you try and lose then it isn't your fault. But if you don't try and we lose, then it's all your fault. 

Kites rise highest against the wind, not with it 

At the end of the day, let there be no excuses, no explanations, no regrets # 

Keep your best wishes, close to your heart and watch what happens 

Never stop dreaming, never stop believing, never give up, never stop trying, and never stop learning 

Whatever the mind can conceive and believe, it can achieve 

If you hang out with chickens, you're going to cluck and if you hang out with eagles, you're going to fly
Supreme excellence consists of breaking the enemy's resistance without fighting. 

Judge your success by what you had to give up in order to get it 

Forget yesterday - it has already forgotten you. Don't sweat tomorrow - you haven't even met. Instead, open your eyes and your heart to a truly precious gift - today

The difference between a successful person and others is not a lack of strength, not a lack of knowledge, but rather a lack in will

In order to succeed, your desire for success should be greater than your fear of failure 

Our greatest fear should not be of failure but of succeeding at things in life that don't really matter 

Always bear in mind that your own resolution to succeed is more important than any one thing


The biggest challenge after success is shutting up about it



A kind gesture can reach a wound that only compassion can heal



You�re not obligated to win. You�re obligated to keep trying. To the best you can do everyday

Rich people have small TVs and big libraries, and poor people have small libraries and big TVs?


The three great essentials to achieve anything worthwhile are, first, hard work; second, stick-to-itiveness



Success does not consist in never making mistakes but in never making the same one a second time


Don't confuse poor decision-making with destiny. Own your mistakes. It�s ok; we all make them. Learn from them so they can empower you!


Never was anything great achieved without danger


Let today be the day you stop being haunted by the ghost of yesterday. Holding a grudge & harboring anger/resentment is poison to the soul. Get even with people...but not those who have hurt us, forget them, instead get even with those who have helped us.



And will you succeed? Yes indeed, yes indeed! Ninety-eight and three-quarters percent guaranteed



Failure is a bend in the road, not the end of the road. Learn from failure and keep moving forward.



The only time you fail is when you fall down and stay down.?



Success is getting what you want. Happiness is wanting what you get.



If you have a dream, don�t just sit there. Gather courage to believe that you can succeed and leave no stone unturned to make it a reality



My past has not defined me, destroyed me, deterred me, or defeated me; it has only strengthened me



Don't mistake activity with achievement.


Successful people have no fear of failure. But unsuccessful people do. Successful people have the resilience to face up to failure�learn the lessons and adapt from it.


Winners are not afraid of losing. But losers are. Failure is part of the process of success. People who avoid failure also avoid success



Eighty percent of success is showing up


The universe doesn�t give you what you ask for with your thoughts - it gives you what you demand with your actions


If I cannot do great things, I can do small things in a great way




Keep your friends for friendship, but work with the skilled and competent



The true measure of success is how many times you can bounce back from failure



When you take risks you learn that there will be times when you succeed and there will be times when you fail, and both are equally important




Success, after all, loves a witness, but failure can't exist without one



How much you can learn when you fail determines how far you will go into achieving your goals



Be an Encourager: When you encourage others, you boost their self-esteem, enhance their self-confidence, make them work harder, lift their spirits and make them successful in their endeavors. Encouragement goes straight to the heart and is always available. Be an encourager. Always??




To be successful you need friends and to be very successful you need enemies



You only have to do a very few things right in your life so long as you don't do too many things wrong



When you live your life by poor standards, you inflict damage on everyone who crosses your path, especially those you love


As Aristotle said, 'Excellence is a habit.' I would say furthermore that excellence is made constant through the feeling that comes right after one has completed a work which he himself finds undeniably awe-inspiring. He only wants to relax until he's ready to renew such a feeling all over again because to him, all else has become absolutely trivial.



The secret to success is constancy of purpose.



If you want to find happiness, find gratitude



If you really want the key to success, start by doing the opposite of what everyone else is doing



Be patient. Your skin took a while to deteriorate. Give it some time to reflect a calmer inner state. As one of my friends states on his Facebook profile: "The true Losers in Life, are not those who Try and Fail, but those who Fail to Try


Whole life is a search for beauty. But, when the beauty is found inside, the search ends and a beautiful journey begins



The power to change your life lies in the simplest of steps





Start with big dreams and make life worth living



Life is a balanced system of learning and evolution. Whether pleasure or pain; every situation in your life serves a purpose. It is up to us to recognize what that purpose could be.



If you want to give yourself a fair chance to succeed, never expect too much too soon



Live in this moment ... for it is the only moment we have



Unless you know where you are going then you will not know how to get there



You are forgiven for your happiness and your successes only if you generously consent to share them



Failure is a greater teacher than success



The backbone of success is...hard work, determination, good planning, and perserverence



Fashion does not have to prove that it is serious. It is the proof that intelligent frivolity can be something creative and positive



Perfection' is man's ultimate illusion. It simply doesn't exist in the universe.... If you are a perfectionist, you are guaranteed to be a loser in whatever you do



Live your vision and demand your success



You are beautiful. Your beauty, just like your capacity for life, happiness, and success, is immeasurable



Strive not to be a success, but rather to be of value



To do more for the world than the world does for you - that is success


?More powerful than the will to win is the courage to begin


Stop trying to 'fix' yourself; you're NOT broken! You are perfectly imperfect and powerful beyond measure



We have forty million reasons for failure, but not a single excuse


The more you can have control over your ego rather than let it run amuck, the more successful you�ll be in all areas of life


Just pick a goal and stick to it? No big complicated secret


Luck is a word the bitter teach to the ignorant



Challenges in life can either enrich you or poison you. You are the one who decides



You become what you think about



Everything is easier said than done. Wanting something is easy. Saying something is easy. The challenge and the reward are in the doing



A person who makes few mistakes makes little progress


Success sometimes may be defined as a disaster put on hold. Qualified. Has to be


Your success and happiness lies in you. Resolve to keep happy, and your joy and you shall form an invicible host against difficulties


Never mind those failures till yesterday. Each new day is a sequel of a wonderful life; gifted with hopes to succeed


You think intelligence and grit can succeed by themselves, but I'm telling you that's a pretty illusion

While others were dreaming about it - I was getting it done



Believe in yourself. Under-confidence leads to a self-fulfilling prophecy that you are not good enough for your work



Remember, results aren't the criteria for success � it's the effort made for achievement that is most important.


Do you want to retire early rich or retire late poor? Cosmic Ordering answers the first question



Never underestimate the power you have to take your life in a new direction.



When we fail, our pride supports us, and when we succeed it betrays us


Do not let your grand ambitions stand in the way of small but meaningful accomplishments



Even if you are on the right track, but just sit there, you will still get run over

For me, Chanel is like music. There are certain notes and you have to make another tune with them


If you have a success you have it for the wrong reasons. If you become popular it is always because of the worst aspects of your work